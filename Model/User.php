<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 * @method findByEmail
 * @method findByEmailhash
 * @method findByEmailhashAndEmaildomain
 * @property Fileobject $Fileobject
 * @property PostulatesUser $PostulatesUser
 * @property RepresentativesUser $RepresentativesUser
 * @property Postulate $Postulate
 * @property Representative $Representative
 *
 */
class User extends AppModel {
	public $tablePrefix='user_';
	public $actsAs=array('Asgraf.Status','Asgraf.Enum','EmailPassword','AsgrafJsonField.Json'=>'metadata');
	public $order='username';

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField='username';


	public function __construct($id=false,$table=null,$ds=null) {
		/**
		 * Validation rules
		 */
		$this->validate=array(
			'username'=>array(
				'notempty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notempty'),
				),
				'minlength'=>array(
					'rule'=>array('minLength',3),
					'message'=>__d('validation','minlength %s','3'),
					'allowEmpty'=>true,
				),
				'maxlength'=>array(
					'rule'=>array('maxLength',32),
					'message'=>__d('validation','maxlength %s','32'),
					'allowEmpty'=>true,
				),
			),
			'password'=>array(
				'notempty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notempty'),
					'required'=>'create',
				),
				'minlength'=>array(
					'rule'=>array('minLength',8),
					'message'=>__d('validation','minlength %s','8'),
					'allowEmpty'=>true,
				),
				'maxlength'=>array(
					'rule'=>array('maxLength',40),
					'message'=>__d('validation','maxlength %s','40'),
					'allowEmpty'=>true,
				),
				'unique'=>array(
					'rule'=>array('isUnique'),
					'message'=>__d('validation','unique'),
					'allowEmpty'=>true,
				),
			),
			'email'=>array(
				'notempty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notempty'),
					'on'=>'create',
				),
				'email'=>array(
					'rule'=>array('email'),
					'message'=>__d('validation','email'),
					'allowEmpty'=>true,
				),
				'minlength'=>array(
					'rule'=>array('minLength',3),
					'message'=>__d('validation','minlength %s','3'),
					'allowEmpty'=>true,
				),
				'maxlength'=>array(
					'rule'=>array('maxLength',100),
					'message'=>__d('validation','maxlength %s','100'),
					'allowEmpty'=>true,
				),
				'unique'=>array(
					'rule'=>array('isUnique'),
					'message'=>__d('validation','unique'),
					'allowEmpty'=>true,
				),
			),
			'status'=>array(
				'notempty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notempty'),
				),
			),
		);
		parent::__construct($id,$table,$ds);
	}

	public $belongsTo = array(
		'ParentUser' => array(
			'className' => 'User',
			'foreignKey' => 'pid',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	public $hasMany=[
		'Fileobject'=>[
			'className'=>'Filerepo.Fileobject',
			'foreignKey'=>'creator_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		],
		'PostulatesUser'=>[
			'className'=>'PostulatesUser',
			'foreignKey'=>'user_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		],
		'RepresentativesUser'=>[
			'className'=>'RepresentativesUser',
			'foreignKey'=>'user_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		],
		'ChildUser'=>[
			'className'=>'User',
			'foreignKey'=>'pid',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		],
	];

	public $hasOne=[
		'ConnectedRepresentative'=>[
			'className'=>'Representative',
			'foreignKey'=>'connected_user_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		],
	];

	public $hasAndBelongsToMany = [
		'Postulate' => [
			'className' => 'Postulate',
			'joinTable' => 'postulates_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'postulate_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		],
		'Representative' => [
			'className' => 'Representative',
			'joinTable' => 'representatives_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'representative_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		]
	];

	public function beforeValidate($options=array()) {
		if(empty($this->data[$this->alias]['password'])) {
			unset($this->data[$this->alias]['password']);
		}
		if(empty($this->data[$this->alias]['email'])) {
			unset($this->data[$this->alias]['email']);
		}
		if(!empty($this->data[$this->alias]['username']) && strlen($this->data[$this->alias]['username'])>32) {
			$this->data[$this->alias]['username']=substr($this->data[$this->alias]['username'],0,32);
		}
		if(empty($this->data)) throw new InternalErrorException();
		if(array_key_exists('avatar_from',$this->data[$this->alias])) {
			if(!in_array($this->data[$this->alias]['avatar_from'],['Google','Facebook'])) {
				$this->data[$this->alias]['avatar_from'] = null;
			}
		}
		return parent::beforeValidate($options);
	}

	public function beforeSave($options=array()) {
		if(isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password']=(new BlowfishPasswordHasher())->hash($this->data[$this->alias]['password']);
		}
		if(!empty($this->data[$this->alias]['email'])) {
			$this->data[$this->alias]['email']=strtolower($this->data[$this->alias]['email']);
			$this->data[$this->alias]['emailhash']=md5($this->data[$this->alias]['email']);
			$this->data[$this->alias]['emaildomain']=getEmailDomain($this->data[$this->alias]['email']);
		}
		if(Hash::get($this->data,$this->alias.'.status')=='deleted') {
			//Usuwamy dane osobowe usera
			$this->data[$this->alias]['username']='Konto usunięte';
			$this->data[$this->alias]['email']=null;
			$this->data[$this->alias]['password']=null;
			$this->data[$this->alias]['avatar_from']='Default';
			$this->data[$this->alias]['metadata']=null;
			$this->data[$this->alias]['admin']=false;
			//i anulujemy oddane głosy
			$user_postulates = $this->PostulatesUser->find('all',[
				'conditions'=>[
					'PostulatesUser.user_id'=>$this->id,
					'PostulatesUser.value !='=>0
				]
			]);
			foreach($user_postulates as $user_postulate) {
				$this->PostulatesUser->id = $user_postulate['PostulatesUser']['id'];
				$this->PostulatesUser->set([
					'postulate_id'=>$user_postulate['PostulatesUser']['postulate_id'],
					'user_id'=>$this->id,
					'value'=>0
				]);
				if(!$this->PostulatesUser->save()) {
					throw new InternalErrorException('Problem z anulowaniem głosów na postulaty');
				}
			}

			$user_representatives = $this->RepresentativesUser->find('all',[
				'conditions'=>[
					'RepresentativesUser.user_id'=>$this->id,
					'RepresentativesUser.value !='=>0
				]
			]);
			foreach($user_representatives as $user_representative) {
				$this->RepresentativesUser->id = $user_representative['RepresentativesUser']['id'];
				$this->RepresentativesUser->set([
					'representative_id'=>$user_representative['RepresentativesUser']['representative_id'],
					'user_id'=>$this->id,
					'value'=>0
				]);
				if(!$this->RepresentativesUser->save()) {
					throw new InternalErrorException('Problem z anulowaniem głosów na reprezentantów');
				}
			}
		}
		return parent::beforeSave($options);
	}

	public function afterSave($created, $options = array()) {
		if($this->id && array_key_exists('representative_id',$this->data[$this->alias])) {
			if(!$this->Representative->updateAll(['connected_user_id'=>null],['connected_user_id'=>$this->id])) {
				throw new InternalErrorException('Problem z odłaczeniem usera od reprezentanta');
			}
			if(!empty($this->data[$this->alias]['representative_id'])) {
				$this->Representative->id = $this->data[$this->alias]['representative_id'];
				if(!$this->Representative->saveField('connected_user_id',$this->id)) {
					throw new InternalErrorException('Problem z powiązaniem usera z reprezentantem');
				}

			}
		}

	}
}
