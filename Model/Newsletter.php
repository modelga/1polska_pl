<?php
App::uses('AppModel', 'Model');
/**
 * Newsletter Model
 * @property User $User
 * @property NewslettersUser $NewslettersUser
 */
class Newsletter extends AppModel {
	public $actsAs=array('Asgraf.Enum');
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField='subject';


	public function __construct($id=false,$table=null,$ds=null) {
		/**
		 * Validation rules
		 */
		$this->validate=array(
			'subject'=>array(
				'notEmpty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notEmpty'),
				),
				'minlength'=>array(
					'rule'=>array('minLength',2),
					'message'=>__d('validation','minlength %s','2'),
					'allowEmpty'=>true,
				),
				'maxlength'=>array(
					'rule'=>array('maxLength',255),
					'message'=>__d('validation','maxlength %s','255'),
					'allowEmpty'=>true,
				),
			),
			'txt'=>array(
				'notEmpty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notEmpty'),
				),
				'minlength'=>array(
					'rule'=>array('minLength',2),
					'message'=>__d('validation','minlength %s','2'),
					'allowEmpty'=>true,
				),
				'maxlength'=>array(
					'rule'=>array('maxLength',65535),
					'message'=>__d('validation','maxlength %s','65535'),
					'allowEmpty'=>true,
				),
			),
			'status'=>array(
				'notEmpty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notEmpty'),
				),
			),
		);
		parent::__construct($id,$table,$ds);
		$this->virtualFields['sent_num']='SELECT count(1) FROM newsletters_users AS NewslettersUser WHERE NewslettersUser.newsletter_id='.$this->alias.'.id AND sent=true';
		$this->virtualFields['sent_max']='SELECT count(1) FROM newsletters_users AS NewslettersUser WHERE NewslettersUser.newsletter_id='.$this->alias.'.id';
	}

	public $hasMany=array(
		'NewslettersUser'=>array(
			'className'=>'NewslettersUser',
			'foreignKey'=>'newsletter_id',
			'dependent'=>true,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		)
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany=array(
		'User'=>array(
			'className'=>'User',
			'with'=>'NewslettersUser',
			'foreignKey'=>'newsletter_id',
			'associationForeignKey'=>'user_id',
			'unique'=>'keepExisting',
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'finderQuery'=>'',
			'deleteQuery'=>'',
			'insertQuery'=>''
		)
	);

	public function beforeValidate($options=array()) {
		if(!$this->id) {
			if(isset($this->data[$this->alias]['status']) && !in_array($this->data[$this->alias]['status'],['draft','unsent'])) {
				$this->invalidate('status',__('Nie można edytować newsletterów które już zostały wysłane'));
			}}
		return parent::beforeValidate($options);
	}
}
