<?php
App::uses('AppModel', 'Model');
/**
 * NewslettersUser Model
 *
 * @property Newsletter $Newsletter
 * @property User $User
 */
class NewslettersUser extends AppModel {

	public function __construct($id=false,$table=null,$ds=null) {
		/**
		 * Validation rules
		 */
		$this->validate=array(
			'sent'=>array(
				'boolean'=>array(
					'rule'=>array('boolean'),
					'message'=>__d('validation','boolean'),
					'allowEmpty'=>true,
				),
				'notEmpty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notEmpty'),
				),
			),
			'newsletter_id'=>array(
				'notEmpty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notEmpty'),
				),
				'numeric'=>array(
					'rule'=>array('numeric'),
					'message'=>__d('validation','numeric'),
					'allowEmpty'=>true,
				),
				'minval'=>array(
					'rule'=>array('comparison','>=',1),
					'message'=>__d('validation','minval %s %s','>=','1'),
					'allowEmpty'=>true,
				),
				'maxval'=>array(
					'rule'=>array('comparison','<=',9223372036854775807),
					'message'=>__d('validation','maxval %s %s','<=','9223372036854775807'),
					'allowEmpty'=>true,
				),
			),
			'user_id'=>array(
				'notEmpty'=>array(
					'rule'=>array('notEmpty'),
					'message'=>__d('validation','notEmpty'),
				),
				'numeric'=>array(
					'rule'=>array('numeric'),
					'message'=>__d('validation','numeric'),
					'allowEmpty'=>true,
				),
				'minval'=>array(
					'rule'=>array('comparison','>=',1),
					'message'=>__d('validation','minval %s %s','>=','1'),
					'allowEmpty'=>true,
				),
				'maxval'=>array(
					'rule'=>array('comparison','<=',9223372036854775807),
					'message'=>__d('validation','maxval %s %s','<=','9223372036854775807'),
					'allowEmpty'=>true,
				),
			),
		);
		parent::__construct($id,$table,$ds);
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo=array(
		'Newsletter'=>array(
			'className'=>'Newsletter',
			'foreignKey'=>'newsletter_id',
			'conditions'=>'',
			'fields'=>'',
			'order'=>''
		),
		'User'=>array(
			'className'=>'User',
			'foreignKey'=>'user_id',
			'conditions'=>'',
			'fields'=>'',
			'order'=>''
		)
	);
	public function beforeValidate($options=array()) {
		return parent::beforeValidate($options);
	}

	public function beforeSave($options=array()) {
		return parent::beforeSave($options);
	}
}
