<?php
/**
 * @var $this View
 */
?>
<div class="newsletters form">
	<?php echo $this->Form->create('Newsletter'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Newsletter'); ?></legend>
		<?php
		echo $this->Form->input('subject', array('label'=>__( 'Subject')));
		echo $this->Form->input('txt', array('label'=>__( 'Content')));
		?>
	</fieldset>
	<?php echo $this->Form->button(__('Submit'), array('class'=>'submit')); ?>
	<?php echo $this->Form->end(); ?>
</div>