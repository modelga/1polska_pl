<?php
/**
 * @var $this View
 */
if(Hash::get($this->viewVars,'flashbox_displayed')) return;
$this->viewVars['flashbox_displayed']=true;
$flashbox_content = '';
foreach(array('flash','auth','good','neutral','bad') as $flashtype) {
	$flashbox_content .= $this->Session->flash($flashtype,array('element'=>'flash','params'=>array('type'=>$flashtype)));
}
echo '<section id="flashbox">'.$flashbox_content.'</section>';