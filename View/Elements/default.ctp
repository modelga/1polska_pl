<?php
/**
 * @var \Cake\View\View $this
 */
if(empty($type)) $type = null;
if(!empty($class) && strpos($class,'error')!==false) $type = 'error';
switch($type) {
	case 'success':
		$title = __('Success');
		$class .= ' alert-success';
		$icon = 'glyphicon-ok-sign';
		break;
	case 'info':
		$title = __('Information');
		$class .= ' alert-info';
		$icon = 'glyphicon-info-sign';
		break;
	case 'error':
		$title = __('Error');
		$class .= ' alert-danger';
		$icon = 'glyphicon-exclamation-sign';
		break;
	default:
		$title = __('Warning');
		$class .= ' alert-warning';
		$icon = 'glyphicon-exclamation-sign';
		break;
}
$this->response->disableCache();
debug(get_defined_vars());
?>
<div class="alert <?=$class?> alert-dismissable fade in">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<span class="glyphicon <?=$icon?>"></span>
	<strong><?=$title?></strong>
	<p><?=$message?></p>
</div>