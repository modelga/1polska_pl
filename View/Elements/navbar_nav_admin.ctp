<?php
App::uses('AuthComponent','Controller/Component');
/**
 * @var $this View
 */
if(AuthComponent::user('id')) {
	$avatar = $this->Html->image(
		getAvatarUrl(AuthComponent::user(),24),
		['width'=>24,'height'=>24]
	);
} else {
	$avatar = $this->Html->image('anonim.jpg',['width'=>24,'height'=>24]);
}
?>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only"><?=__('Toggle navigation')?></span>
				<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">1Polska.pl</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><?=$this->Html->link('Użytkownicy',['plugin'=>false,'controller'=>'users'])?></li>
				<li><?=$this->Html->link('Postulaty',['plugin'=>false,'controller'=>'postulates'])?></li>
				<li><?=$this->Html->link('Reprezentanci',['plugin'=>false,'controller'=>'representatives'])?></li>
				<li><?=$this->Html->link('Newslettery',['plugin'=>false,'controller'=>'newsletters'])?></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$avatar.' '.h(AuthComponent::user('username'))?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><?=$this->Html->link('Podgląd profilu',['admin'=>false,'controller'=>'users','action'=>'view','emailhash'=>AuthComponent::user('emailhash'),'slug'=>slug(AuthComponent::user('username'))])?></li>
							<li><?=$this->Html->link('Edycja profilu',['admin'=>false,'controller'=>'users','action'=>'edit'])?></li>
							<li><?=$this->Html->link('Wyloguj',['admin'=>false,'controller'=>'users','action'=>'logout'])?></li>
							<li class="divider"></li>
							<li><?=$this->Html->link('Wyjdź z panelu admina',['admin'=>false,'controller'=>'home','action'=>'main'])?></li>
						</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>
