<?php
/**
 * @var $this View
 */
if(empty($type)) $type = null;
switch($type) {
	case 'good':
		$title = __('Sukces');
		$class = 'alert-success';
		$icon = 'icon-ok-circled';
		break;
	case 'neutral':
		$title = __('Informacja');
		$class = 'alert-info';
		$icon = 'icon-info-circled';
		break;
	case 'bad':
		$title = __('Błąd');
		$class = 'alert-danger';
		$icon = 'icon-attention-circled';
		break;
	default:
		$title = __('Ostrzeżenie');
		$class = 'alert-warning';
		$icon = 'icon-attention-circled';
		break;
}
$this->response->disableCache();
?>
<div class="alert <?=$class?> alert-dismissable fade in">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<span class="icon-container"><i class="<?=$icon?>"></i></span>
	<strong><?=$title?></strong>
	<p><?=$message?></p>
</div>