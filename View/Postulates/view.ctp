<?php
/**
 * @var $this View
 * @var $postulate
 */
$edit_time = strtotime($postulate['Postulate']['created'])+15*MINUTE;
$this->set('title_for_layout',$postulate['Postulate']['name']);
$this->Html->script('jquery.canvasjs.min.js',array('block'=>'script'));
$this->Html->script('vote_history',array('block'=>'script'));
if(AuthComponent::user('admin')) {
	echo $this->Html->link('Edycja',array('admin'=>true,'action'=>'edit','id'=>$this->request['id'],'slug'=>$this->request['slug']),array('class'=>'btn btn-info'));
} elseif(AuthComponent::user('id')==$postulate['Postulate']['creator_user_id'] && $edit_time>time()) {
	echo $this->Html->link('Edycja',array('action'=>'edit','id'=>$this->request['id'],'slug'=>$this->request['slug']),array('class'=>'btn btn-info'));
}
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="postulates view">
<h1><?php  echo h($postulate['Postulate']['name']); ?></h1>
	<?php
	echo '<p>'.$this->element('postulate_vote',array('postulate'=>$postulate)).'</p>';
	echo $this->element('social_buttons');
	?>
	<dl>
		<dt><?php echo __('Krótki opis'); ?></dt>
		<dd class="description"><?php echo h($postulate['Postulate']['description']); ?></dd>
		<dt><?php echo __('Pełny opis'); ?></dt>
		<dd class="content" data-markdown><?php echo h($postulate['Postulate']['content']); ?></dd>
		<dt><?php echo __('Utworzono'); ?></dt>
		<dd class="created"><?php echo $this->Format->datetime($postulate['Postulate']['created']);?></dd>
		<dt><?php echo __('Ostatnia modyfikacja'); ?></dt>
		<dd class="modified"><?php echo $this->Format->datetime($postulate['Postulate']['modified']);?></dd>
	</dl>
	<?php if(!empty($relatedRepresentatives)) {?>
	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Reprezentanci popierający ten postulat:</h3>
				<ul style="-webkit-column-width: 300px;-moz-column-width: 300px;column-width: 300px;">
					<?php
					foreach($relatedRepresentatives as $cu_representative) {
						$url = ['controller'=>'representatives','action'=>'view','id'=>$cu_representative['id'],'slug'=>slug($cu_representative['name'])];
						echo '<li><a href="'.$this->Html->url($url).'">';
						if(!empty($cu_representative['Photo']['id'])) {
							echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$cu_representative['Photo']['id'],'access_key'=>$cu_representative['Photo']['access_key'],16,16,1),array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
						} else {
							echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
						}
						echo '&nbsp;';
						echo $cu_representative['name'];
						echo '</a></li>';
					}
					?>
				</ul>
			</div>
		</div>
	</section>
	<?php } ?>
	<div id="vote_history"></div>
	<script>
		var vote_history_url = '<?=$this->Html->url(array('controller'=>'PostulatesUsersChanges','action'=>'index','ext'=>'json','?'=>['postulate_id'=>$this->request['id']]))?>';
		var vote_history_key = 'Postulates';
	</script>
</div>