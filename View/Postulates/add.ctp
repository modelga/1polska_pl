<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Nowy Postulat');
?>

<p>
Postulat powinien być jak najbardziej czytelny i jednoznaczny, żeby nie było wątpliwości czego dotyczy i co jest jego istotą. 
<br>Postulat należy dobrze przemyśleć i przygotować (zredagować jego treść) przed dodaniem - ponieważ nie ma możliwości późniejszej edycji treści.
</p>

<div class="postulates form">
	<?php echo $this->Form->create('Postulate'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Postulat');?></legend>
		<?php
		echo $this->Form->input('name',array('label'=>__('Tytuł'),'maxlength'=>50));
		echo $this->Form->input('description',array('label'=>__('Krótki opis'),'maxlength'=>160));
		echo $this->Form->input('content',array('label'=>__('Pełny opis'),'data-markdown-editor'));
		echo $this->element('recaptcha');
		?>
	</fieldset>
	<?php echo $this->Form->button(__('Wyślij'),array('class'=>'btn btn-default submit','data-icon'=>'ok')); ?>
	<?php echo $this->Form->end(); ?>
</div>