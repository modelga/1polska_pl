<?php
/**
 * @var $this View
 * @var $title_for_layout
 * @var $description_for_layout
 * @var $representatives
 * @var $postulates
 * @var $users_count
 */
$this->set('title_for_layout','1Polska.pl - Strona główna');
$width = $height = 80;
$join_button = AuthComponent::user('id')?'':$this->Html->link('Przyłącz się »',['controller'=>'users','action'=>'register'],['class'=>'btn btn-primary btn-lg pull-right']);
$this->append('header','
<div class="jumbotron" style="padding:10px">
	<div class="container">
		<h5 style="font-style:italic; font-weight:normal; color:#99BB99">„Do triumfu zła wystarczy bierność ludzi dobrych” – Edmund Burke</h5>
		<h1>Czas zjednoczyć dobrych ludzi wobec patologii obecnego systemu!</h1>
		<h4>Chcesz żyć w normalnym kraju? Masz dość absurdów IIIRP? Nie jesteś sam!</h4>
		<p style="font-size:15px">To my, polskie społeczeństwo, które ma już dość tego chorego systemu, jaki stworzyły nam tzw. "elity IIIRP". Pookrągłostołowemu, zabetonowanemu układowi partyjnemu i obecnej klasie politycznej już dziękujemy! Dość hipokryzji, obłudy i zakłamania polityków oraz mediów. Tu ustalimy wspólnie priorytety zmian i wreszcie połączymy siły żeby przywrócić w Polsce normalność. To my: normalni, zwykli ludzie chcący żyć uczciwie i godnie jesteśmy w Polsce większością - i czas to sobie uświadomić!</p>
		<h2 class="text-center">'.$this->Html->link('Zobacz więcej »',['controller'=>'pages','action'=>'display','o_co_chodzi'],['class'=>'btn btn-primary btn-lg pull-left']).'<strong style="margin-left:15px; margin-right:15px; font-weight:normal">Jest nas już <span style="color:#CC3344">'.number_format ($users_count,0,',','&nbsp;').'</span> osób!</strong>
		'.$join_button.'
		</h2>
	</div>
</div>
');
?>
<section class="container">
	<div class="row">
	</div>
	<div class="row">
		<h2><?=$this->Html->link('PRIORYTETY ZMIAN - najpopularniejsze postulaty (to co nas łączy):',['controller'=>'postulates','action'=>'index','status'=>'active'])?></h2>
		<?php
		foreach($postulates as $postulate) {
			$url = ['controller'=>'postulates','action'=>'view','id'=>$postulate['Postulate']['id'],'slug'=>slug($postulate['Postulate']['name'])];
			echo '<div class="col-md-4 h210">';
			echo '<div class="w140 truncate" data-click-url="'.$this->Html->url($url).'">';
			echo '<h3>'.$this->Html->link($postulate['Postulate']['name'],$url).'</h3>';
			echo $postulate['Postulate']['description'];
			echo '</div>';
			echo '<p class="clearfix">';
			echo $this->element('postulate_vote',array('postulate'=>$postulate)).'&nbsp;';
			echo '</p>';
			echo '</div>';
		}
		?>
		<div><?=$this->Html->link('Zobacz propozycje użytkowników',['controller'=>'postulates','action'=>'index','status'=>'active'],['class'=>'btn btn-primary']);?></div>
	</div>
	<div class="row">
		<h2><?=$this->Html->link('REPREZENTANCI i liderzy - osoby popierające tę inicjatywę:',['controller'=>'representatives','action'=>'index'])?></h2>
		<div style="margin-top:10px; margin-bottom:12px; margin-left:28px; margin-right:58px; padding:5px 25px; background-color:#DDEEDD; border-color:#999999; border-radius:20px">
		<h4><strong style="color:#00AA00">Uwaga! Na liście reprezentantów znajdują się wyłącznie osoby, które popierają tę inicjatywę (same się zgłosiły lub zgodziły się być na tej liście) - ZAPRASZAM wszystkich dobrych ludzi do przyłączenia się.</strong></h4>
<h5 style="color:#669966">Reprezentantów zaproponowanych samowolnie przez użytkowników można znaleźć <a href="http://1polska.pl/reprezentanci/propozycje_uzytkownikow">TUTAJ</a> - przekonajmy te osoby, żeby włączyły się w inicjatywę zjednoczenia polskiego społeczeństwa. <a href="https://www.youtube.com/watch?v=D-Rq0hRldbE">ZAPRASZAM!</a></h5>
</div>
		<?php
		foreach($representatives as $representative) {
			$url = ['controller'=>'representatives','action'=>'view','id'=>$representative['Representative']['id'],'slug'=>slug($representative['Representative']['name'])];
			echo '<div class="col-md-4 h210">';
			echo '<div class="max140 truncate" data-click-url="'.$this->Html->url($url).'">';
			if($representative['Photo']['id']) {
				echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],$width,$height,1),array('class'=>'pull-left img-circle','width'=>$width,'height'=>$height));
			} else {
				echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>$width,'height'=>$height));
			}
			echo '<h3>'.$this->Html->link($representative['Representative']['name'],$url).'</h3>';
			echo $representative['Representative']['description'];
			echo '</div>';
			echo '<p class="clearfix">';
			echo $this->element('representative_vote',array('representative'=>$representative)).'&nbsp;';
			echo '</p>';
			echo '</div>';
		}
		?>
		<div><?=$this->Html->link('Zobacz wszystkich reprezentantów',['controller'=>'representatives','action'=>'index','status'=>'active'],['class'=>'btn btn-primary']);?></div>
	</div>
</section>
