<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>
<section class="tags index">
	<?php if(empty($tags)) {
		 if(!$this->request['requested']) echo __('No items to show');
	} else { 
		echo '<h2>'.__('Tags').'</h2>';
?>
<table>
	<tr>
		<?php if(!$this->request->query('name')) { ?>
		<th class="name"><?php echo !empty($all)?__('Name'):$this->Paginator->sort('name',__('Name')); ?></th>
		<?php } ?>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tags as $tag) { ?>
	<tr>
		<?php if(!$this->request->query('name')) { ?>
		<td class="name">
			<?php echo $this->Html->link($tag['Tag']['name'],array('action'=>'view','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'])); ?>
		</td>
		<?php } ?>
		<td class="actions">
			<?php echo $this->Html->link(__('Clone'),array('action'=>'add','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'clone','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'copy')); ?>
			<?php echo $this->Html->link(__('View'),array('action'=>'view','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id']),array('class'=>'view','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'eye-open')); ?>
			<?php echo $this->Html->link(__('Edit'),array('action'=>'edit','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'edit')); ?>
			<?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'],'?'=>array('redirect'=>$_SERVER['REQUEST_URI'])),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'trash'),__('Are you sure you want to delete %s tag?',$tag['Tag']['name'])); ?>
		</td>
	</tr>
<?php }?>
</table>
<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__('New Tag'),array('action'=>'add','?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'add','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
