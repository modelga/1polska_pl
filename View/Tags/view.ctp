<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="tags view">
<h2><?php  echo __('Tag'); ?></h2>
	<dl>
		<dt><?php echo __('Parent Tag'); ?></dt>
		<dd class="parent_id">
			<?php echo $this->Html->link($tag['ParentTag']['name'],array('controller'=>'tags','action'=>'view','slug'=>slug($tag['ParentTag']['name']),'id'=>$tag['ParentTag']['id'])); ?>
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd class="name">
			<?php echo $this->Html->link($tag['Tag']['name'],array('action'=>'view','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'])); ?>
		</dd>
	</dl>
</div>
<nav class="actions">
	<?php echo $this->Html->link(__('Edit Tag'),array('action'=>'edit','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-icon'=>'edit')); ?>
	<?php echo $this->Form->postLink(__('Delete Tag'),array('action'=>'delete','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id']),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-icon'=>'trash'),__('Are you sure you want to delete %s tag?',$tag['Tag']['name'])); ?>
</nav>
