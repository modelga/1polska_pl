<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="tags form">
<?php echo $this->Form->create('Tag'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Tag');?></legend>
	<?php
		echo $this->Form->input('parent_id',array('label'=>__('Parent')));
		echo $this->Form->input('name',array('label'=>__('Name')));
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'),array('class'=>'submit','data-icon'=>'ok')); ?>
<?php echo $this->Form->end(); ?>
</div>
<nav class="actions">
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($this->Form->value('Tag.name')),'id'=>$this->Form->value('Tag.id')),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-icon'=>'trash'),__('Are you sure you want to delete %s tag?',$this->Form->value('Tag.name'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tags'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?></li>
	</ul>
</nav>
