<?php
/**
 * @var $this View
 * @var $representative
 */
$this->set('title_for_layout',$representative['Representative']['name']);
$this->Html->script('jquery.canvasjs.min.js',array('block'=>'script'));
$this->Html->script('vote_history',array('block'=>'script'));
$connected_sign = Hash::get($representative,'ConnectedUser.status')=='active'?'<span class="text-success glyphicon glyphicon-ok" aria-hidden="true" title="Ten reprezentant posiada aktywne konto użytkownika na 1polska.pl"></span> ':'';
if(AuthComponent::user('admin')) {
	echo $this->Html->link('Edycja',array('admin'=>true,'action'=>'edit','id'=>$this->request['id'],'slug'=>$this->request['slug']),array('class'=>'btn btn-info'));
} elseif(isset($representative['Representative']['connected_user_id'])) {
	if(AuthComponent::user('id')==$representative['Representative']['connected_user_id']) {
		echo $this->Html->link('Edycja',array('action'=>'edit','id'=>$this->request['id'],'slug'=>$this->request['slug']),array('class'=>'btn btn-info'));
	}
} elseif(AuthComponent::user('id')==$representative['Representative']['creator_user_id']) {
	echo $this->Html->link('Edycja',array('action'=>'edit','id'=>$this->request['id'],'slug'=>$this->request['slug']),array('class'=>'btn btn-info'));
}

?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="representatives view">
	<h1><?php echo $connected_sign.h($representative['Representative']['name']); ?></h1>
	<p class="representative_photo">
		<?php
		if($representative['Photo']['id']) {
			echo $this->Html->image(
				array('plugin'=>'filerepo','controller'=>'fileobjects','action'=>'view','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],'filename'=>$representative['Photo']['name']),
				array('class'=>'img-thumbnail')
			);
		} else {
			echo $this->Html->image('anonim.jpg',array('class'=>'img-thumbnail'));
		}
		?>
	</p>
	<?php
	echo '<p>'.$this->element('representative_vote',array('representative'=>$representative)).'</p>';
	echo $this->element('social_buttons');
	?>

	<dl>
		<dt><?php echo __('Krótki opis'); ?></dt>
		<dd class="description"><?php echo $this->Format->string($representative['Representative']['description']); ?></dd>
		<dt><?php echo __('Pełny opis'); ?></dt>
		<dd class="content" data-markdown><?php echo $this->Format->text($representative['Representative']['content']); ?></dd>
		<?php if(empty($representative['ConnectedUser']['id'])) {?>
			<dt><?php echo __('Utworzono'); ?></dt>
			<dd class="created"><?php echo $this->Format->datetime($representative['Representative']['created']); ?> przez <?php echo $this->Html->link($this->Html->image(
						getAvatarUrl($representative['CreatorUser'],20),
						['width'=>20,'height'=>20]
					).' '.($representative['CreatorUser']['username']),
					['controller'=>'users','action'=>'view','slug'=>slug($representative['CreatorUser']['username']),'emailhash'=>$representative['CreatorUser']['emailhash']],
					['escape'=>false]
				); ?></dd>
			<dt><?php echo __('Ostatnia modyfikacja'); ?></dt>
			<dd class="modified"><?php echo $this->Format->datetime($representative['Representative']['modified']); ?> przez <?php echo $this->Html->link($this->Html->image(
						getAvatarUrl($representative['ModifierUser'],20),
						['width'=>20,'height'=>20]
					).' '.($representative['ModifierUser']['username']),
					['controller'=>'users','action'=>'view','slug'=>slug($representative['ModifierUser']['username']),'emailhash'=>$representative['ModifierUser']['emailhash']],
					['escape'=>false]
				); ?></dd>
		<?php } else { ?>
			<dt><?php echo __('Data rejestracji'); ?></dt>
			<dd class="registered"><?php echo $this->Format->datetime($representative['ConnectedUser']['created']); ?></dd>
		<?php } ?>
	</dl>
	<?php if(!empty($representative['ConnectedUser']['id'])) {?>
		<section class="container">
			<div class="row">
				<div class="col-md-6">
					<h3>Reprezentanci, którym ufam:</h3>
					<ul>
						<?php
						$count = 0;
						foreach($representative['ConnectedUser']['Representative'] as $cu_representative) {
							if($cu_representative['RepresentativesUser']['value']==1) {
								$url = ['controller'=>'representatives','action'=>'view','id'=>$cu_representative['id'],'slug'=>slug($cu_representative['name'])];
								echo '<li><a href="'.$this->Html->url($url).'">';
								if(!empty($cu_representative['Photo']['id'])) {
									echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$cu_representative['Photo']['id'],'access_key'=>$cu_representative['Photo']['access_key'],16,16,1),array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
								} else {
									echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
								}
								echo '&nbsp;';
								echo $cu_representative['name'];
								echo '</a></li>';
								$count++;
							}
						}
						if(!$count) {
							echo '<li>Brak</li>';
						}
						?>
					</ul>
				</div>
				<div class="col-md-6">
					<h3>Reprezentanci, którym nie ufam:</h3>
					<ul>
						<?php
						$count = 0;
						foreach($representative['ConnectedUser']['Representative'] as $cu_representative) {
							if($cu_representative['RepresentativesUser']['value']==-1) {
								$url = ['controller'=>'representatives','action'=>'view','id'=>$cu_representative['id'],'slug'=>slug($cu_representative['name'])];
								echo '<li class=""><a href="'.$this->Html->url($url).'">';
								if(!empty($representative['Photo']['id'])) {
									echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$cu_representative['Photo']['id'],'access_key'=>$cu_representative['Photo']['access_key'],16,16,1),array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
								} else {
									echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
								}
								echo '&nbsp;';
								echo $cu_representative['name'];
								echo '</a></li>';
								$count++;
							}
						}
						if(!$count) {
							echo '<li>Brak</li>';
						}
						?>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3>Postulaty, które popieram:</h3>
					<ul style="-webkit-column-width: 300px;-moz-column-width: 300px;column-width: 300px;">
						<?php
						$count = 0;
						foreach($representative['ConnectedUser']['Postulate'] as $postulate) {
							if($postulate['PostulatesUser']['value']==1) {
								$url = ['controller'=>'postulates','action'=>'view','id'=>$postulate['id'],'slug'=>slug($postulate['name'])];
								echo '<li class=""><a href="'.$this->Html->url($url).'">';
								echo $postulate['name'];
								echo '</a></li>';
								$count++;
							}
						}
						if(!$count) {
							echo '<li>Brak</li>';
						}
						?>
					</ul>
				</div>
			</div>
		</section>
	<?php } ?>
	<div id="vote_history"></div>
	<script>
		var vote_history_url = '<?=$this->Html->url(array('controller'=>'RepresentativesUsersChanges','action'=>'index','ext'=>'json','?'=>['representative_id'=>$this->request['id']]))?>';
		var vote_history_key = 'Representatives';
	</script>
</div>