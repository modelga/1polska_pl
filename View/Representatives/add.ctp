<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Nowy Reprezentant');
?>

<p style="color:#990000">
<strong>Dodaj SIEBIE do listy reprezentantów</strong>
<br>Jeśli popierasz tę inicjatywę i chcesz ją reprezentować, propagować lub się w nią zaangażować - przyłącz się jako reprezentant.
<br>Jeśli myślisz o zaproponowaniu kogoś innego niż siebie jako reprezentanta - <strong>zaproponuj to najpierw tej osobie</strong> i przekonaj ją żeby sama się przyłaczyła.
</p>

<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="representatives form">
	<?php echo $this->Form->create('Representative',array('role'=>'form','type'=>'file')); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Reprezentant');?></legend>
		<?php
		echo $this->Form->input('firstname',array('','label'=>__('Imię'),'maxlength'=>20));
		echo $this->Form->input('lastname',array('label'=>__('Nazwisko'),'maxlength'=>20));
		echo $this->Form->input('photo',array('type'=>'file','label'=>__('Zdjęcie')));
		echo $this->Form->input('description',array('label'=>__('Krótki opis'),'maxlength'=>160));
		echo $this->Form->input('content',array('label'=>__('Pełny opis'),'data-markdown-editor'));
		echo $this->element('recaptcha');
		?>
	</fieldset>
	<?php echo $this->Form->button(__('Wyślij'),array('class'=>'btn btn-default submit','data-icon'=>'ok')); ?>
	<?php echo $this->Form->end(); ?>
</div>
