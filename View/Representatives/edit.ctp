<?php
/**
 * @var $this View
 * @var $edit_time
 */
$edit_time = strtotime($this->Form->value('Representative.created'))+15*MINUTE;
$edit_time_expired = $edit_time<time();
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="representatives form">
<?php echo $this->Form->create('Representative',array('role'=>'form','type'=>'file')); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Reprezentant');?></legend>
	<?php
	echo $this->Form->input('firstname', array('', 'label'=>__('Imię'), 'maxlength'=>20,'disabled'=>$edit_time_expired));
	echo $this->Form->input('lastname', array('label'=>__('Nazwisko'), 'maxlength'=>20,'disabled'=>$edit_time_expired));
	echo $this->Form->input('photo',array('type'=>'file','label'=>__('Zdjęcie')));
	echo $this->Form->input('description',array('label'=>__('Krótki opis'),'maxlength'=>160));
	echo $this->Form->input('content',array('label'=>__('Treść'),'data-markdown-editor'));
	echo $this->element('recaptcha');
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'),array('class'=>'submit','data-icon'=>'ok')); ?>
<?php echo $this->Form->end(); ?>
</div>