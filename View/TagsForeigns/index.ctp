<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>
<section class="tagsForeigns index">
	<?php if(empty($tagsForeigns)) {
		 if(!$this->request['requested']) echo __('No items to show');
	} else { 
		echo '<h2>'.__('Tags Foreigns').'</h2>';
?>
<table>
	<tr>
		<?php if(!$this->request->query('tag_id')) { ?>
		<th class="tag_id"><?php echo !empty($all)?__('Tag'):$this->Paginator->sort('tag_id',__('Tag')); ?></th>
		<?php } ?>
		<?php if(!$this->request->query('modelname')) { ?>
		<th class="modelname"><?php echo !empty($all)?__('Modelname'):$this->Paginator->sort('modelname',__('Modelname')); ?></th>
		<?php } ?>
		<?php if(!$this->request->query('foreign_key')) { ?>
		<th class="foreign_key"><?php echo !empty($all)?__('Foreign Key'):$this->Paginator->sort('foreign_key',__('Foreign Key')); ?></th>
		<?php } ?>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tagsForeigns as $tagsForeign) { ?>
	<tr>
		<?php if(!$this->request->query('tag_id')) { ?>
		<td class="tag_id">
			<?php echo $this->Html->link($tagsForeign['Tag']['name'],array('controller'=>'tags','action'=>'view','slug'=>slug($tagsForeign['Tag']['name']),'id'=>$tagsForeign['Tag']['id'])); ?>
		</td>
		<?php } ?>
		<?php if(!$this->request->query('modelname')) { ?>
		<td class="modelname"><?php echo $this->Format->modelname($tagsForeign['TagsForeign']['modelname']); ?></td>
		<?php } ?>
		<?php if(!$this->request->query('foreign_key')) { ?>
		<td class="foreign_key"><?php echo $this->Format->biginteger($tagsForeign['TagsForeign']['foreign_key']); ?></td>
		<?php } ?>
		<td class="actions">
			<?php echo $this->Html->link(__('Clone'),array('action'=>'add','slug'=>slug($tagsForeign['TagsForeign']['id']),'id'=>$tagsForeign['TagsForeign']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'clone','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'copy')); ?>
			<?php echo $this->Html->link(__('View'),array('action'=>'view','slug'=>slug($tagsForeign['TagsForeign']['id']),'id'=>$tagsForeign['TagsForeign']['id']),array('class'=>'view','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'eye-open')); ?>
			<?php echo $this->Html->link(__('Edit'),array('action'=>'edit','slug'=>slug($tagsForeign['TagsForeign']['id']),'id'=>$tagsForeign['TagsForeign']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'edit')); ?>
			<?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($tagsForeign['TagsForeign']['id']),'id'=>$tagsForeign['TagsForeign']['id'],'?'=>array('redirect'=>$_SERVER['REQUEST_URI'])),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'trash'),__('Are you sure you want to delete %s tagsForeign?',$tagsForeign['TagsForeign']['id'])); ?>
		</td>
	</tr>
<?php }?>
</table>
<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__('New Tags Foreign'),array('action'=>'add','?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'add','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
