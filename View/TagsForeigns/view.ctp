<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="tagsForeigns view">
<h2><?php  echo __('Tags Foreign'); ?></h2>
	<dl>
		<dt><?php echo __('Tag'); ?></dt>
		<dd class="tag_id">
			<?php echo $this->Html->link($tagsForeign['Tag']['name'],array('controller'=>'tags','action'=>'view','slug'=>slug($tagsForeign['Tag']['name']),'id'=>$tagsForeign['Tag']['id'])); ?>
		</dd>
		<dt><?php echo __('Modelname'); ?></dt>
		<dd class="modelname"><?php echo $this->Format->modelname($tagsForeign['TagsForeign']['modelname']); ?></dd>
		<dt><?php echo __('Foreign Key'); ?></dt>
		<dd class="foreign_key"><?php echo $this->Format->biginteger($tagsForeign['TagsForeign']['foreign_key']); ?></dd>
	</dl>
</div>
<nav class="actions">
	<?php echo $this->Html->link(__('Edit Tags Foreign'),array('action'=>'edit','slug'=>slug($tagsForeign['TagsForeign']['id']),'id'=>$tagsForeign['TagsForeign']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-icon'=>'edit')); ?>
	<?php echo $this->Form->postLink(__('Delete Tags Foreign'),array('action'=>'delete','slug'=>slug($tagsForeign['TagsForeign']['id']),'id'=>$tagsForeign['TagsForeign']['id']),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-icon'=>'trash'),__('Are you sure you want to delete %s tagsForeign?',$tagsForeign['TagsForeign']['id'])); ?>
	<?php echo $this->Html->link(__('List Tags Foreigns'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?>
	<?php echo $this->Html->link(__('New Tags Foreign'),array('action'=>'add'),array('class'=>'add','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
