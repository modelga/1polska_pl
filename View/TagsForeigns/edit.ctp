<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="tagsForeigns form">
<?php echo $this->Form->create('TagsForeign'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('TagsForeign');?></legend>
	<?php
		echo $this->Form->input('tag_id',array('label'=>__('Tag')));
		echo $this->Form->input('modelname',array('options'=>$modelname,'label'=>__('Modelname')));
		echo $this->Form->input('foreign_key',array('label'=>__('Foreign Key')));
	?>
	</fieldset>
<?php echo $this->Form->button(__('Submit'),array('class'=>'submit','data-icon'=>'ok')); ?>
<?php echo $this->Form->end(); ?>
</div>
<nav class="actions">
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($this->Form->value('TagsForeign.id')),'id'=>$this->Form->value('TagsForeign.id')),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-icon'=>'trash'),__('Are you sure you want to delete %s tagsForeign?',$this->Form->value('TagsForeign.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tags Foreigns'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?></li>
	</ul>
</nav>
