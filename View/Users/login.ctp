<?php
/**
 * @var $this View
 * @var $title_for_layout
 */
$this->set('title_for_layout','Logowanie');
if(!$this->request->is('ssl')) {
	?>
	<div class="alert alert-danger fade in">
		<span class="icon-container"><i class="icon-attention-circled"></i></span>
		<strong>Uwaga</strong>
		<p>Korzystasz z nieszyfrowanej wersji naszego serwisu. <br />Jeśli logujesz się z kafejki internetowej, hotelu lub innego niezaufanego miejsca twoje połączenie z naszą stroną może zostać podsłuchane.</p>
		<?=$this->Html->link('Przejdź do bezpiecznego logowania',str_replace('http://','https://',Router::url(['action'=>'login','full_base'=>true])),['class'=>'btn btn-success']);?>
	</div>
<?php
}
?>

<br><p style="color:#BBBBBB">Aby kontynuować - Zaloguj się przez swoje konto społecznościowe:</p><br>

<?=$this->Html->link($this->Html->image('Google.png').' Zaloguj za pomocą Google',['admin'=>false,'plugin'=>false,'controller'=>'home','action'=>'social_login','google'],['escape'=>false,'class'=>'btn btn-default'])?>

<?=$this->Html->link($this->Html->image('Facebook.png').' Zaloguj za pomocą Facebook',['admin'=>false,'plugin'=>false,'controller'=>'home','action'=>'social_login','facebook'],['escape'=>false,'class'=>'btn btn-default'])?>

<br><br><p style="color:#BBBBBB">lub skorzystaj z formularza poniżej, jeśli nie masz / nie chcesz używać konta społecznościowego:</p><br>

<div class="row">
	<div class="col-md-5">
		<ul class="nav nav-tabs">
			<li role="presentation" class="active"><?=$this->Html->link(__d('user','Login'),['action'=>'login']);?></a></li>
			<li role="presentation"><?=$this->Html->link(__d('user','Register'),['action'=>'register']);?></a></li>
		</ul>
		<div>
			<?php echo $this->Form->create('User', ['url'=>['action'=>'login']]); ?>
			<fieldset>
				<?php
				echo $this->element('flashbox');
				echo $this->Form->input('email', ['label'=>[__d('user','Email'),'class'=>'col col-md-3 control-label no-error'],'error'=>false,'class'=>'form-control no-error']);
				echo $this->Form->input('password', ['label'=>__d('user','Password'),'value'=>false]);
				echo $this->element('recaptcha');
				?>
				<div class="form-group">
					<label class="col col-md-3 control-label"></label>
					<div class="col col-md-9">
						<div class="submit">
							<?php
							if($this->request->is('ssl')) {
								echo $this->Form->button('<span class="glyphicon glyphicon glyphicon-lock text-success" aria-hidden="true"></span> Zaloguj', ['type'=>'submit','label'=>'','class'=>'btn btn-success','escape'=>false,'title'=>'Bezpieczne logowanie']);
							} else {
								echo $this->Form->button('Zaloguj', ['type'=>'submit','label'=>'','class'=>'btn btn-primary','escape'=>false,'title'=>'Logowanie bez szyfrowania']);
							}
							?>

						</div>
					</div>
				</div>
				<?=$this->Html->link('Zapomniałeś hasła?',['controller'=>'users','action'=>'forgot_password'],['class'=>'pull-right']);?>
			</fieldset>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
Logowanie oznacza akceptację <?=$this->Html->link('polityki prywatności',['controller'=>'pages','action'=>'display','polityka_prywatnosci'])?>
