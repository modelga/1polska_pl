<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>
<section class="users index">
	<?php if(empty($users)) {
		 if(!$this->request['requested']) echo __('No items to show');
	} else { 
		echo '<h2>'.__d('user','Users').'</h2>';
?>
<table>
	<tr>
		<?php if(!$this->request->query('username')) { ?>
		<th class="username"><?php echo !empty($all)?__d('user','Username'):$this->Paginator->sort('username',__d('user','Username')); ?></th>
		<?php } ?>
		<?php if(!$this->request->query('status')) { ?>
		<th class="status"><?php echo !empty($all)?__d('user','Status'):$this->Paginator->sort('status',__d('user','Status')); ?></th>
		<?php } ?>
		<?php if(!$this->request->query('created')) { ?>
		<th class="created"><?php echo !empty($all)?__d('user','Created'):$this->Paginator->sort('created',__d('user','Created')); ?></th>
		<?php } ?>
		<th class="actions"><?php echo __d('user','Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user) { ?>
	<tr>
		<?php
		if(!$this->request->query('username')) {
			$avatar = $this->Html->image(
				getAvatarUrl($user['User'],20),
				['width'=>20,'height'=>20]
			);
			?>
		<td class="username">
			<?php echo $this->Html->link($avatar.' '.h($user['User']['username']),array('action'=>'view','slug'=>slug($user['User']['username']),'emailhash'=>$user['User']['emailhash']),array('escape'=>false)); ?>
		</td>
		<?php } ?>
		<?php if(!$this->request->query('status')) { ?>
		<td class="status"><?php echo $this->Format->enum($user['User']['status']); ?></td>
		<?php } ?>
		<?php if(!$this->request->query('created')) { ?>
		<td class="created"><?php echo $this->Format->datetime($user['User']['created']); ?></td>
		<?php } ?>
		<td class="actions">
			<?php echo $this->Html->link(__('View'),array('action'=>'view','slug'=>slug($user['User']['username']),'emailhash'=>$user['User']['emailhash']),array('class'=>'view','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'eye-open')); ?>
		</td>
	</tr>
<?php }?>
</table>
<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
