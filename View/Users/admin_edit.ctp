<?php
/**
 * @var $this View
 * @var $user
 * @var $status
 * @var $representatives
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="users form">
	<?php echo $this->Form->create('User'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__d('user','User');?></legend>
		<?php
		echo $this->Form->input('username',array('label'=>__d('user','Username')));
		echo $this->Form->input('password',array('required'=>false,'value'=>false,'label'=>__d('user','New Password')));
		echo $this->Form->input('email',array('required'=>false,'value'=>false,'label'=>__d('user','New Email')));
		echo $this->Form->input('status',array('options'=>$status,'label'=>__d('user','Status')));
		echo $this->Form->input('representative_id',array('empty'=>'-==Brak==-','options'=>$representatives,'label'=>'Reprezentant','value'=>Hash::get($user,'ConnectedRepresentative.id')));
		echo $this->element('recaptcha');
		?>
	</fieldset>
	<?php echo $this->Form->button(__d('user','Submit'),array('class'=>'submit','data-icon'=>'ok')); ?>
	<?php echo $this->Form->end(); ?>
</div>
<nav class="actions">
	<ul>
		<li><?php echo $this->Html->link(__d('user','List Users'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?></li>
	</ul>
</nav>