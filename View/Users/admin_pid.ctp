<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>
	<section class="users index">
<?php if(empty($users)) {
	if(!$this->request['requested']) echo __('No items to show');
} else {
	foreach ($users as $user) {
		?>
		<table class="table well">
			<thead>
			<tr>
				<th class="id"><?=__d('user','ID'); ?></th>
				<?php if(!$this->request->query('username')) { ?>
					<th class="username"><?=__d('user','Username'); ?></th>
				<?php } ?>
				<th class="email"><?php echo __('Email'); ?></th>
				<?php if(!$this->request->query('status')) { ?>
					<th class="status"><?=__d('user','Status'); ?></th>
				<?php } ?>
				<?php if(!$this->request->query('created')) { ?>
					<th class="created"><?=__d('user','Created'); ?></th>
				<?php } ?>
				<?php if(!$this->request->query('modified')) { ?>
					<th class="modified"><?=__d('user','Modified'); ?></th>
				<?php } ?>
				<th class="actions"><?=__d('user','Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="id"><?=$user['User']['id'];?></td>
				<?php if(!$this->request->query('username')) { ?>
					<td class="username">
						<?php
						$avatar = $this->Html->image(getAvatarUrl($user['User'],20),['width'=>20,'height'=>20]);
						?>

						<?php echo $avatar.$this->Html->link($user['User']['username'],array('admin'=>false,'action'=>'view','slug'=>slug($user['User']['username']),'emailhash'=>$user['User']['emailhash']),array('escape'=>false)); ?>
					</td>
					<td class="email"><?=$this->Html->link($user['User']['email'],'mailto:'.$user['User']['email']);?></td>
				<?php } ?>
				<?php if(!$this->request->query('status')) { ?>
					<td class="status"><?php echo $this->Format->enum($user['User']['status']); ?></td>
				<?php } ?>
				<?php if(!$this->request->query('created')) { ?>
					<td class="created"><?php echo $this->Format->datetime($user['User']['created']); ?></td>
				<?php } ?>
				<?php if(!$this->request->query('modified')) { ?>
					<td class="modified"><?php echo $this->Format->datetime($user['User']['modified']); ?></td>
				<?php } ?>
				<td class="actions">
					<?php echo $this->Html->link(__('Edit'),array('action'=>'edit','slug'=>slug($user['User']['username']),'id'=>$user['User']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'edit')); ?>
					<?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($user['User']['username']),'id'=>$user['User']['id'],'?'=>array('redirect'=>$_SERVER['REQUEST_URI'])),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'trash'),__d('user','Are you sure you want to delete %s user?',$user['User']['username'])); ?>
				</td>
			</tr>
			<?php foreach ($user['ChildUser'] as $childuser) { ?>
				<tr>
					<td class="id"><?=$childuser['id'];?></td>
					<?php if(!$this->request->query('username')) { ?>
						<td class="username">
							<?php
							$avatar = $this->Html->image(getAvatarUrl($childuser,20),['width'=>20,'height'=>20]);
							?>
							<?php echo $avatar.$this->Html->link($childuser['username'],array('admin'=>false,'action'=>'view','slug'=>slug($childuser['username']),'emailhash'=>$childuser['emailhash']),array('escape'=>false)); ?>
						</td>
						<td class="email"><?=$this->Html->link($childuser['email'],'mailto:'.$childuser['email']);?></td>
					<?php } ?>
					<?php if(!$this->request->query('status')) { ?>
						<td class="status"><?php echo $this->Format->enum($childuser['status']); ?></td>
					<?php } ?>
					<?php if(!$this->request->query('created')) { ?>
						<td class="created"><?php echo $this->Format->datetime($childuser['created']); ?></td>
					<?php } ?>
					<?php if(!$this->request->query('modified')) { ?>
						<td class="modified"><?php echo $this->Format->datetime($childuser['modified']); ?></td>
					<?php } ?>
					<td class="actions">
						<?php echo $this->Html->link(__('Edit'),array('action'=>'edit','slug'=>slug($childuser['username']),'id'=>$childuser['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'edit')); ?>
						<?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($childuser['username']),'id'=>$childuser['id'],'?'=>array('redirect'=>$_SERVER['REQUEST_URI'])),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'trash'),__d('user','Are you sure you want to delete %s user?',$childuser['username'])); ?>
					</td>
				</tr>
			<?php }?>
			</tbody>
		</table>
	<?php }?>
	<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
	</section>
<?php }?>