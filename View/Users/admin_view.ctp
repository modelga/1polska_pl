<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="users view">
<h2><?php  echo __d('user','User'); ?></h2>
	<dl>
		<dt><?php echo __d('user','Username'); ?></dt>
		<dd class="username">
			<?php echo $this->Html->link($this->Format->username($user['User']['username'],$user['User']),array('admin'=>false,'action'=>'view','slug'=>slug($user['User']['username']),'emailhash'=>$user['User']['emailhash']),array('escape'=>false)); ?>
		</dd>
		<dt><?php echo __d('user','Email'); ?></dt>
		<dd class="email"><?php echo $this->Format->email($user['User']['email']); ?></dd>
		<dt><?php echo __d('user','Admin'); ?></dt>
		<dd class="admin"><?php echo $this->Format->boolean($user['User']['admin']); ?></dd>
		<dt><?php echo __d('user','Status'); ?></dt>
		<dd class="status"><?php echo $this->Format->enum($user['User']['status']); ?></dd>
		<dt><?php echo __d('user','Created'); ?></dt>
		<dd class="created"><?php echo $this->Format->datetime($user['User']['created']); ?></dd>
		<dt><?php echo __d('user','Modified'); ?></dt>
		<dd class="modified"><?php echo $this->Format->datetime($user['User']['modified']); ?></dd>
	</dl>
</div>
<nav class="actions">
	<?php echo $this->Html->link(__d('user','Edit User'),array('action'=>'edit','slug'=>slug($user['User']['username']),'id'=>$user['User']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-icon'=>'edit')); ?>
	<?php echo $this->Form->postLink(__d('user','Delete User'),array('action'=>'delete','slug'=>slug($user['User']['username']),'id'=>$user['User']['id']),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-icon'=>'trash'),__d('user','Are you sure you want to delete %s user?',$user['User']['username'])); ?>
	<?php echo $this->Html->link(__d('user','List Users'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?>
	<?php echo $this->Html->link(__d('user','New User'),array('action'=>'add'),array('class'=>'add','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
