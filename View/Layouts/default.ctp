<?php
/**
 * @var $this View
 */
if(empty($page_id)) $page_id='';
if(empty($page_class)) $page_class='';
$adminpanel = AuthComponent::user('admin')&&$this->request['admin'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" id="<?=$page_id?>" class="<?=$page_class?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title><?=$this->fetch('title');?></title>
	<?php
	if($this->fetch('description')) echo $this->Html->meta('description',$this->fetch('description'));
	echo $this->Html->meta('icon',$this->Html->url('/favicon.png'));
	echo $this->fetch('meta');
	echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.1/modernizr.min.js');
	echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js');
	echo $this->Html->css('main');
	echo $this->Html->css('bootstrap-markdown.min.css');
	echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.min.css');
	echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2-bootstrap.css');
	echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css');
	echo $this->Html->css('konrad');
	echo $this->fetch('css');
	?>
</head>
<body>
<header>
	<?=$this->element($adminpanel?'navbar_nav_admin':'navbar_nav')?>
	<?=$this->fetch('header'); ?>
</header>
<div id="fb-root"></div>
<div class="container">
	<?=$this->element('flashbox')?>
	<nav class="breadcrumbs"><?=$this->Html->getCrumbList(['class'=>'breadcrumb'],'Strona główna');?></nav>
	<div class="view_content"><?=$this->fetch('content'); ?></div>
	<nav class="breadcrumbs"><?=$this->Html->getCrumbList(['class'=>'breadcrumb'],'Strona główna');?></nav>
</div>
<footer id="footer" data-role="footer">
	<p class="text-muted">
	<?=$this->Html->link('Polityka prywatności',['controller'=>'pages','action'=>'display','polityka_prywatnosci'],['class'=>'text-muted']);?>
	:: &copy; <?=ucfirst($this->request->host());?> <?=date('Y');?> ::
	<?=$this->Html->link('Zgłoś błąd lub sugestię',['controller'=>'pages','action'=>'display','disqus'],['class'=>'text-muted']);?>
	</p>

</footer>
<?php
echo $this->Html->script('util.js');
echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js');
echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js');
echo $this->Html->script('apply_active');
echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js');
echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.min.js');
echo $this->Html->script('apply_select2');
//echo $this->Html->script('apply_datalist');
echo $this->Html->script('markdown');
echo $this->Html->script('to-markdown');
echo $this->Html->script('apply_markdown');
echo $this->Html->script('bootstrap-markdown');
echo $this->Html->script('apply_click_url');
echo $this->fetch('script');
?>
</body>
</html>
