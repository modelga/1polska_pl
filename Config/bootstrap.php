<?php
require APP . '/Vendor/autoload.php';
spl_autoload_unregister(array('App', 'load'));
spl_autoload_register(array('App', 'load'), true, true);
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 */
$engine = 'File';
// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => $engine));

Cache::config('ip_to_host', array(
	'engine' => $engine,
	'prefix' => 'rdns_',
	'path' => CACHE . 'rdns' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => '+999 days',
));

Cache::config('vote_history', array(
	'engine' => $engine,
	'prefix' => 'vote_history_',
	'path' => CACHE,
	'serialize' => ($engine === 'File'),
	'duration' => '+1 minute',
));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
CakePlugin::load('Crud');
/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter . By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 * 		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

function generatePassword ($length=8){
	$password = "";
	$i = 0;
	$possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	while ($i<$length) {
		$char = substr($possible,mt_rand(0,strlen($possible)-1),1);
		$password .= $char;
		$i++;
	}
	return $password;
}

function gethostbyaddr_cached($ip) {
	return Cache::remember(preg_replace("/[^0-9]/",'_',$ip),function() use ($ip) {
		return gethostbyaddr($ip);
	},'ip_to_host');
}

function getAvatarUrl($user,$size=null,$avatar_from=null){
	//debug($user);
	if(!array_key_exists('emailhash',$user) && !array_key_exists('email',$user) || !array_key_exists('metadata',$user)){
		throw new InternalErrorException('missing email or metadata');
	}
	if(empty($avatar_from)) {
		if(!array_key_exists('avatar_from',$user)) throw new InternalErrorException('missing avatar_from');
		$avatar_from = $user['avatar_from'];
	}

	if($avatar_from=='Facebook') {
		$facebook_id = Hash::get($user,'metadata.Facebook.raw.id');
		if($facebook_id>0) {
			return "https://graph.facebook.com/$facebook_id/picture?width=$size";
		}
	}
	if($avatar_from=='Google') {
		$google_picture = Hash::get($user,'metadata.Google.raw.picture');
		if(!empty($google_picture)) {
			return "$google_picture?sz=$size";
		}
	}
	$hash = isset($user['emailhash'])?$user['emailhash']:md5($user['email']);
	if($avatar_from=='Default') {
		$get = http_build_query(array('size'=>$size,'default'=>'identicon','f'=>'y'));
		return "https://www.gravatar.com/avatar/$hash?$get";
	}
	$get = http_build_query(array('size'=>$size,'default'=>'identicon','rating'=>'g'));
	return "https://www.gravatar.com/avatar/$hash?$get";
}

function getEmailDomain($email) {
	return strtolower(explode('@',$email,2)[1]);
}

if(Configure::read('debug')) {
	CakePlugin::load('DebugKit');
	App::uses('FireCake', 'DebugKit.Lib');
}
CakePlugin::load('Asgraf',array('bootstrap'=>true));
CakePlugin::load('AsgrafMessage');
CakePlugin::load('AsgrafJsonField');
CakePlugin::load('AsgrafSearch');
CakePlugin::load('Filerepo',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('BoostCake');
CakePlugin::load('ExtAuth');