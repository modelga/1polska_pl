<?php
App::uses('AppController', 'Controller');
/**
 * Representatives Controller
 *
 * @property Representative $Representative
 * @property PaginatorComponent $Paginator
 */
class RepresentativesController extends AppController {
	public $guestActions=array('index','view','vote_history');
	public $components = [
		'Crud.Crud' => [
			'actions' => [
				'add'=>'Crud.Add',
				'index'=>'Crud.Index',
				'edit'=>'Crud.Edit',
				'view'=>'Crud.View',
				'admin_index'=>'Crud.Index',
				'admin_add'=>'Crud.Add',
				'admin_edit'=>'Crud.Edit',
				'admin_view'=>'Crud.View',
				'admin_delete'=>'Crud.Delete',
			]
		]
	];

	public function add() {
		$last = $this->Representative->find('first',array(
			'order'=>'created DESC',
			'conditions'=>[
				'creator_user_id'=>AuthComponent::user('id')
			]
		));
		if($last && strtotime($last['Representative']['created'])+MONTH>time()) {
			return $this->Message->flash(__('Zaproponowanie kolejnego reprezentanta będzie możliwe nie wcześniej niż: %s (Można proponować jednego reprezentanta na miesiąc)',prettyDateTime(strtotime($last['Representative']['created'])+MONTH)),$this->referer(array('action'=>'index'),true),'bad');
		}
		if(strtotime(AuthComponent::user('created'))+DAY>time()) {
			return $this->Message->flash(__('Zaproponowanie reprezentanta będzie możliwe nie wcześniej niż: %s (Można proponować reprezentanta najwcześniej dobę po rejestracji)',prettyDateTime(strtotime(AuthComponent::user('created'))+DAY)),$this->referer(array('action'=>'index'),true),'bad');
		}
		if($this->request->is(['put','post'])) {
			$conditions=[];
			$tmp=[];
			foreach(explode(' ',preg_replace("/[^a-zA-ZążźśęćńłóĄŻŹŚĘĆŃŁÓ]/", ' ',trim($this->request->data('Representative.firstname')))) as $firstname) {
				$tmp['OR']['name LIKE']='%'.$firstname.'%';
			}
			$conditions[]=$tmp;
			$tmp=[];
			foreach(explode(' ',preg_replace("/[^a-zA-ZążźśęćńłóĄŻŹŚĘĆŃŁÓ]/", ' ',trim($this->request->data('Representative.lastname')))) as $lastname) {
				$tmp['OR']['name LIKE']='%'.$lastname.'%';
			}
			$conditions[]=$tmp;
			$duplicate = $this->Representative->find('first', ['conditions'=>$conditions]);
			if(!empty($duplicate)) {
				if($duplicate['Representative']['status']=='deleted') {
					return $this->Message->flash(__('Reprezentant %s został już wcześniej usunięty i nie można go dodać ponownie. Po więcej informacji proszę o kontakt z administracją',$duplicate['Representative']['name']),['action'=>'index'],'bad');
				} else {
					return $this->Message->flash(__('Reprezentant %s już istnieje',$duplicate['Representative']['name']),['action'=>'view','id'=>$duplicate['Representative']['id'],'slug'=>slug($duplicate['Representative']['name'])],'flash');
				}
			}
		}
		$this->Crud->on('afterSave', function(CakeEvent $event) {
			if($event->subject->success) {
				$this->redirect(['action'=>'view','id'=>$event->subject->id]);
			}
		});
		$this->Crud->execute();
	}

	public function edit() {
		$this->Crud->on('beforeFind', function(CakeEvent $event) {
			$event->subject->query['conditions']['Representative.status']=['active','not_active'];
		});
		$this->Crud->on('afterFind', function(CakeEvent $event) {
			$representative = $event->subject()->item;
			if($representative['Representative']['connected_user_id']) {
				if($representative['Representative']['connected_user_id']!=AuthComponent::user('id')) {
					throw new ForbiddenException();
				}
			} else {
				if($representative['Representative']['creator_user_id']!=AuthComponent::user('id')) {
					throw new ForbiddenException();
				}
			}
		});
		$this->Crud->on('afterSave', function(CakeEvent $event) {
			if($event->subject->success) {
				$this->renewUserSession();
			}
		});
		$this->Crud->execute();
	}


	public function index() {
		if(empty($this->request['status'])) {
			throw new InternalErrorException('empty status');
		}
		$this->Paginator->settings += [
			'order'=>'Representative.upvotes DESC,Representative.downvotes ASC',
			'contain'=>[
				'Photo',
				'ConnectedUser'=>[
					'fields'=>['id','status'],
					'conditions'=>[
						'ConnectedUser.status'=>['active']
					],
				],
			],
			'conditions'=>[
				'Representative.status'=>$this->request['status']
			]
		];
		return $this->Crud->execute();
	}

	public function view() {
		$this->Crud->on('beforeFind', function(CakeEvent $event) {
			$event->subject->query['conditions']['Representative.status']=['active','not_active'];
			$event->subject->query['contain']=[
				'CreatorUser',
				'ModifierUser',
				'Photo',
				'ConnectedUser'=>[
					'fields'=>['id','username','status','created'],
					'conditions'=>[
						'ConnectedUser.status'=>['active','not_active']
					],
					'Postulate'=>[
						'fields'=>['id','name'],
						'conditions'=>[
							'Postulate.status'=>['active','not_active']
						],
						'order'=>'upvotes DESC',
					],
					'Representative'=>[
						'fields'=>['id','name'],
						'conditions'=>[
							'Representative.status'=>['active','not_active'],
							'Representative.id !='=>$this->request['id']
						],
						'order'=>'upvotes DESC',
						'Photo'
					],
				],
			];
		});
		return $this->Crud->execute();
	}

	private function _vote($value,$id=null) {
		$this->Representative->contain([
			'RepresentativesUser'=>[
				'conditions'=>[
					'RepresentativesUser.user_id'=>AuthComponent::user('id'),
				]
			]
		]);
		$representative = $this->Representative->read(null,$id);
		if(empty($representative)) {
			throw new NotFoundException('representative not found');
		}
		if(!in_array($representative['Representative']['status'],['active','not_active'])) {
			throw new NotFoundException('representative not found');
		}
		if(empty($representative['RepresentativesUser'][0]['id'])) {
			$this->Representative->RepresentativesUser->create();
		} else {
			$wait = strtotime($representative['RepresentativesUser'][0]['modified'])+MINUTE-time();
			if($wait>0) {
				return $this->Message->flash(__('Przed ponowną zmianą swojego głosu musisz odczekać jeszcze %s sekund (Swój głos można zmieniać raz na minutę)',$wait),$this->referer(array('action'=>'index'),true),'bad');
			}

			$this->Representative->RepresentativesUser->id = $representative['RepresentativesUser'][0]['id'];
		}
		if(!$this->Representative->RepresentativesUser->save(array(
			'user_id'=>AuthComponent::user('id'),
			'representative_id'=>$id,
			'value'=>$value
		))) {
			throw new InternalErrorException('RepresentativesUser save error');
		}
		$this->renewUserSession();
		if($value) {
			if($value>0) $value = '+'.$value;
			return $this->Message->flash(__('Oddano głos %s na "%s"',$value,h($representative['Representative']['name'])),$this->referer(array('action'=>'index'),true));
		} else {
			return $this->Message->flash(__('Anulowano głos na "%s"',h($representative['Representative']['name'])),$this->referer(array('action'=>'index'),true));
		}
	}

	public function upvote($id=null) {
		if($this->request->method()=='GET' && AuthComponent::user('id')) {
			return $this->Message->flash(__('Zostałeś pomyślnie zalogowany. Teraz możesz oddać swój głos'),array('action'=>'view','id'=>$id));
		}
		$this->request->allowMethod('post');
		return $this->_vote(1,$id);
	}

	public function downvote($id=null) {
		if($this->request->method()=='GET' && AuthComponent::user('id')) {
			return $this->Message->flash(__('Zostałeś pomyślnie zalogowany. Teraz możesz oddać swój głos'),array('action'=>'view','id'=>$id));
		}
		$this->request->allowMethod('post');
		return $this->_vote(-1,$id);
	}

	public function cancelvote($id=null) {
		if($this->request->method()=='GET' && AuthComponent::user('id')) {
			return $this->Message->flash(__('Zostałeś pomyślnie zalogowany. Teraz możesz oddać swój głos'),array('action'=>'view','id'=>$id));
		}
		$this->request->allowMethod('post');
		return $this->_vote(0,$id);
	}
}