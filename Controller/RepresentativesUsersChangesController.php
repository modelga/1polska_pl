<?php
App::uses('AppController','Controller');
/**
 * RepresentativesUsersChanges Controller
 * @property RepresentativesUsersChange $RepresentativesUsersChange
 * @property PaginatorComponent $Paginator
 * @property array paginate
 */
class RepresentativesUsersChangesController extends AppController {
	public $guestActions = ['index'];
	public $components = [
		'Crud.Crud' => [
			'actions' => [
				'index'=>'Crud.Index',
			],
			'listeners'=>[
				'Api',
				'ApiPagination'
			]
		]
	];

	public function index() {
		$this->Paginator->settings['limit']=1000;
		if(!$this->request->query('sort') && !$this->request->query('direction')) {
			$limit = $this->request->query('limit')?:Hash::get($this->Paginator->settings,'limit');
			$this->response->etag(count($limit));
			if ($this->response->checkNotModified($this->request)) {
				return $this->response;
			}
		}
		$this->Paginator->settings['order']='RepresentativesUsersChange.created';
		$this->Paginator->settings['fields']=[
			'RepresentativesUsersChange.value',
			'RepresentativesUsersChange.host',
			'RepresentativesUsersChange.created',
			'RepresentativesUser.representative_id',
			'RepresentativesUser.user_id',
			'User.emailhash',
		];
		$this->Paginator->settings['joins']=[
			[
				'table' => 'representatives_users',
				'alias' => 'RepresentativesUser',
				'type' => 'LEFT',
				'conditions' => [
					'RepresentativesUser.id = RepresentativesUsersChange.representatives_user_id',
				]
			],
			[
				'table' => 'user_users',
				'alias' => 'User',
				'type' => 'LEFT',
				'conditions' => [
					'User.id = RepresentativesUser.user_id',
				]
			]
		];
		$representative_id = $this->request->query('representative_id');
		if($representative_id) {
			$this->Paginator->settings['conditions']['RepresentativesUser.representative_id']=$representative_id;
		}

		$this->Crud->on('afterPaginate', function(CakeEvent $event) {
			if($this->request['ext']) {
				$changes = $event->subject()->items;
				$limit = $this->request->query('limit')?:Hash::get($this->Paginator->settings,'limit');
				if(!$this->request->query('sort') && !$this->request->query('direction')) {
					$this->response->etag(count($changes));
					if(count($changes)==$limit) {
						$this->response->expires('+1 day');
						$this->response->sharable(true,YEAR);
					}
				} else {
					$this->response->etag(md5(json_encode($changes)));
					$this->response->expires('+1 minute');
					$this->response->sharable(true,MINUTE);
				}
				if($this->response->checkNotModified($this->request)) {
					return $this->response;
				}
			}
		});
		return $this->Crud->execute();
	}
}
