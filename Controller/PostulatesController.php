<?php
App::uses('AppController', 'Controller');

/**
 * Postulates Controller
 *
 * @property Postulate $Postulate
 * @property PaginatorComponent $Paginator
 */
class PostulatesController extends AppController {
	public $guestActions=array('index','view','vote_history');
	public $components = [
		'Crud.Crud' => [
			'actions' => [
				'add'=>'Crud.Add',
				'index'=>'Crud.Index',
				'edit'=>'Crud.Edit',
				'view'=>'Crud.View',
				'admin_index'=>'Crud.Index',
				'admin_add'=>'Crud.Add',
				'admin_edit'=>'Crud.Edit',
				'admin_view'=>'Crud.View',
				'admin_delete'=>'Crud.Delete',
			]
		]
	];

	public function add() {
		if(!AuthComponent::user('ConnectedRepresentative.id')) {
			return $this->Message->flash(__('Nowe postulaty mogą proponować tylko ci użytkownicy którzy zgłosili się na reprezentanta'),$this->referer(array('action'=>'index'),true),'bad');
		}
		$last = $this->Postulate->find('first',array(
			'order'=>'created DESC',
			'conditions'=>[
				'creator_user_id'=>AuthComponent::user('id')
			]
		));
		if($last && strtotime($last['Postulate']['created'])+DAY>time()) {
			return $this->Message->flash(__('Zaproponowanie kolejnego postulatu będzie możliwe nie wcześniej niż: %s (Można proponować jeden postulat na dobę)',prettyDateTime(strtotime($last['Postulate']['created'])+DAY)),$this->referer(array('action'=>'index'),true),'bad');
		}
		if(strtotime(AuthComponent::user('created'))+DAY>time()) {
			return $this->Message->flash(__('Zaproponowanie postulatu będzie możliwe nie wcześniej niż: %s (Można proponować postulat najwcześniej dobę po rejestracji)',prettyDateTime(strtotime(AuthComponent::user('created'))+DAY)),$this->referer(array('action'=>'index'),true),'bad');
		}
		$this->Crud->on('afterSave', function(CakeEvent $event) {
			if($event->subject->success) {
				$this->redirect(['action'=>'view','id'=>$event->subject->id]);
			}
		});
		$this->Crud->execute();
	}

	public function edit() {
		$this->Crud->on('beforeFind', function(CakeEvent $event) {
			$event->subject->query['conditions']['Postulate.status']=['active','not_active'];
		});
		$this->Crud->on('afterFind', function(CakeEvent $event) {
			$postulate = $event->subject()->item;
			if($postulate['Postulate']['creator_user_id']!=AuthComponent::user('id')) {
				throw new ForbiddenException('Tylko autor może edytować');
			}
			$edit_time = strtotime($postulate['Postulate']['created'])+15*MINUTE;
			if($edit_time<time()) {
				$this->Message->flash(__('Nie możesz już edytować tego postulatu. Postulat można edytować tylko przez 15 minut od jego utworzenia'),array('action'=>'view','id'=>$postulate['Postulate']['id'],'slug'=>slug($postulate['Postulate']['name'])),'bad');
			} else {
				$this->Message->flash(__('Postulaty można edytować tylko przez 15 minut od ich utworzenia. Możliwość dokonywania poprawek w tym postulacie wygaśnie: %s',prettyDateTime($edit_time)),null,'flash');
			}
		});
		$this->Crud->execute();
	}

	public function index() {
		if($this->vote_overflow()) return null;
		if(empty($this->request['status'])) {
			throw new InternalErrorException('empty status');
		}
		$best = $this->Postulate->find('first',array(
			'order'=>'vote_count DESC'
		));
		$this->set('max_vote_count', Hash::get($best,'Postulate.upvotes')?:1);
		$this->Paginator->settings += [
			'order'=>'Postulate.upvotes DESC',
			'conditions'=>[
				'Postulate.status'=>$this->request['status']
			]
		];
		return $this->Crud->execute();
	}

	private function vote_overflow() {
		if(AuthComponent::user('postulate_vote_count')<=12) return false;
		$postulates = $this->Postulate->PostulatesUser->find('all',[
			'conditions'=>[
				'PostulatesUser.user_id'=>AuthComponent::user('id'),
				'PostulatesUser.value'=>1,
			],
			'contain'=>'Postulate'
		]);
		$this->set('postulates',$postulates);
		$this->view = 'vote_overflow';
		return true;
	}

	public function view() {
		$this->Crud->on('beforeFind', function(CakeEvent $event) {
			$event->subject->query['conditions']['Postulate.status']=['active','not_active'];
			$event->subject->query['contain']=[
				'CreatorUser',
				'ModifierUser',
			];
		});
		$this->Crud->on('afterFind', function(CakeEvent $event) {
			if($event->subject()->success) {
				$this->set('relatedRepresentatives',$this->Postulate->getRelatedRepresentatives($this->request['id']));
			}
		});
		return $this->Crud->execute();
	}

	private function _vote($value,$id=null) {
		$this->Postulate->contain([
			'PostulatesUser'=>[
				'conditions'=>[
					'PostulatesUser.user_id'=>AuthComponent::user('id'),
				]
			]
		]);
		$postulate = $this->Postulate->read(null,$id);
		if(empty($postulate)) {
			throw new NotFoundException('postulate not found');
		}
		if(!in_array($postulate['Postulate']['status'],['active','not_active'])) {
			throw new NotFoundException('representative not found');
		}
		if(empty($postulate['PostulatesUser'][0]['id'])) {
			$this->Postulate->PostulatesUser->create();
		} else {
			$wait = strtotime($postulate['PostulatesUser'][0]['modified'])+MINUTE-time();
			if($wait>0) {
				return $this->Message->flash(__('Przed ponowną zmianą swojego głosu musisz odczekać jeszcze %s sekund (Swój głos można zmieniać raz na minutę)',$wait),$this->referer(array('action'=>'index'),true),'bad');
			}
			$this->Postulate->PostulatesUser->id = $postulate['PostulatesUser'][0]['id'];
		}
		if(!$this->Postulate->PostulatesUser->save(array(
			'user_id'=>AuthComponent::user('id'),
			'postulate_id'=>$id,
			'value'=>$value
		))) {
			throw new InternalErrorException('PostulatesUser save error');
		}
		$this->renewUserSession();
		if($value) {
			if($value>0) $value = '+'.$value;
			return $this->Message->flash(__('Oddano głos %s na "%s"',$value,h($postulate['Postulate']['name'])),$this->referer(array('action'=>'index'),true));
		} else {
			return $this->Message->flash(__('Anulowano głos na "%s"',h($postulate['Postulate']['name'])),$this->referer(array('action'=>'index'),true));
		}
	}

	public function upvote($id=null) {
		if(AuthComponent::user('postulate_vote_count')>=12) {
			return $this->Message->flash(__('Można poprzeć maksymalnie 12 postulatów. Aby móc poprzeć ten postulat musisz wcześniej odebrać swój głos innemu postulatowi.'),$this->referer(array('action'=>'view','id'=>$id),true),'bad');
		}
		if($this->request->method()=='GET' && AuthComponent::user('id')) {
			return $this->Message->flash(__('Zostałeś pomyślnie zalogowany. Teraz możesz oddać swój głos'),array('action'=>'view','id'=>$id));
		}
		$this->request->allowMethod('post');
		return $this->_vote(1,$id);
	}

	public function cancelvote($id=null) {
		if($this->request->method()=='GET' && AuthComponent::user('id')) {
			return $this->Message->flash(__('Zostałeś pomyślnie zalogowany. Teraz możesz oddać swój głos'),array('action'=>'view','id'=>$id));
		}
		$this->request->allowMethod('post');
		return $this->_vote(0,$id);
	}
}
