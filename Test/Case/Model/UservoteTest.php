<?php
App::uses('Uservote', 'Model');

/**
 * Uservote Test Case
 *
 */
class UservoteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.uservote',
		'app.user',
		'app.postulate',
		'app.creator_user',
		'app.modifier_user',
		'app.postulates_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Uservote = ClassRegistry::init('Uservote');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Uservote);

		parent::tearDown();
	}

}
