<?php
App::uses('Postulate', 'Model');

/**
 * Postulate Test Case
 *
 */
class PostulateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.postulate',
		'app.creator_user',
		'app.modifier_user',
		'app.user',
		'app.postulates_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Postulate = ClassRegistry::init('Postulate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Postulate);

		parent::tearDown();
	}

}
