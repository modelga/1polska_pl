<?php
App::uses('RepresentativesUsersChange', 'Model');

/**
 * RepresentativesUsersChange Test Case
 *
 */
class RepresentativesUsersChangeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.representatives_users_change',
		'app.representatives_user',
		'app.user',
		'app.fileobject',
		'app.thumb',
		'app.filepart',
		'app.representative'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RepresentativesUsersChange = ClassRegistry::init('RepresentativesUsersChange');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RepresentativesUsersChange);

		parent::tearDown();
	}

}
